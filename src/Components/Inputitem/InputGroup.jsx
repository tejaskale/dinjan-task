import React from 'react';
import Styles from './Input.module.scss';

const InputGroup = () => {
    return (
        <div class={Styles.container}>
        <form>
        <input type="search" placeholder=""/>
        <div className={Styles.search}></div>
        </form>
        </div>

    );
};

export default InputGroup;
