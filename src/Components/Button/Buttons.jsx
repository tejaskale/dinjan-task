import React from 'react'
import Styles from "./Button.module.scss"
import {BsArrowRight} from "react-icons/bs"


function Buttons({buttonStyle="dark" , innerText}) {
  return (
     <button className={`${Styles[buttonStyle]}`} >
            {innerText}
            <BsArrowRight className={Styles.buttonArrow} />
        </button> 
  )
}

export default Buttons