import   {Fragment , React}from 'react';
import Styles from "./CardStyle.module.scss"
import product1 from "../../assets/product1.png"
import zeroStars from "../../assets/zerostars.png"
import fivestars from "../../assets/fiveStars.png"

function Card({productName,starRating , productImg}) {
  return (
    <Fragment>
        <div className={Styles.container} >
            <div className={Styles.imageWrap} >
                <img src={productImg} className={Styles.productImg} alt="" />
            </div>
            <div className={Styles.productDescription} >
                <p>{productName}</p>
                <img src={starRating}  alt="" />
                <p> ₹300.00</p>
            </div>

        </div>
    </Fragment>
  )
}

export default Card;