import React, { Fragment } from 'react'
import Styles from "./CircleStyle.module.scss"
import letterPad from "../../assets/letterPad.png"

function Circle({image,label,circleColor}) {
  return (
    <Fragment>
        <div>
            <div className={`${Styles[circleColor]}`}>
              <img src={image} alt="" />
              <p>
              {label}
              </p>
            </div>
          </div>
    </Fragment>
  )
}

export default Circle;