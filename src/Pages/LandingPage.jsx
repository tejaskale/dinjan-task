import React, { Fragment } from "react";
import Styles from "./LandingPageStyles.module.scss";
import InputGroup from "../Components/Inputitem/InputGroup";
import backGroundImage from "../assets/backgroundImage.png"
import loginLogo from "../assets/Vector.png";
import { CgAdidas } from "react-icons/cg";
import companyName from "../assets/companyName.png";
import { AiOutlineHeart, AiOutlineShoppingCart } from "react-icons/ai";
import { BsWallet2 } from "react-icons/bs";
import Buttons from "../Components/Button/Buttons";
import bgImageDarksec from "../assets/bgImgDarkSec.png";
import Card from "../Components/Card/Card";
import product1 from "../assets/product1.png";
import product2 from "../assets/product2.png";
import product3 from "../assets/product3.png";
import product4 from "../assets/product4.png";
import zeroStars from "../assets/zerostars.png";
import fiveStars from "../assets/fiveStars.png";
import letterPad from "../assets/letterPad.png";
import circle2 from "../assets/circle2.png";
import circle3 from "../assets/circle3.png";
import circle4 from "../assets/circle4.png";
import Circle from "../Components/workProcessCircles/Circle";
import bgImagesec5 from "../assets/bgImageSec5.png";
import bestPriceLogo from "../assets/bestPriceLogo.png";
import qualityAssure from "../assets/qualityAssuranceLogo.png";
import saveTimeLogo from "../assets/saveTimeLogo.png";
import mrktAsrLogo from "../assets/marketImportLogo.png";
import promoSecImage from "../assets/promoSecImg.png";

function LandingPage() {
  return (
    <Fragment>
      <section className={Styles.headerInfo}>
        <div className={Styles.companyContacts}>
          <div className={Styles.infoAndLogoContainer}>
            <span className={Styles.headerInfoLogo}>
              <svg
                width="16"
                height="12"
                viewBox="0 0 16 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M1.17692 12C0.863077 12 0.588462 11.8788 0.353077 11.6365C0.117692 11.3942 0 11.1115 0 10.7885V1.21154C0 0.888461 0.117692 0.605769 0.353077 0.363462C0.588462 0.121154 0.863077 0 1.17692 0H14.5154C14.8292 0 15.1038 0.121154 15.3392 0.363462C15.5746 0.605769 15.6923 0.888461 15.6923 1.21154V10.7885C15.6923 11.1115 15.5746 11.3942 15.3392 11.6365C15.1038 11.8788 14.8292 12 14.5154 12H1.17692ZM7.84615 6.825L1.17692 2.32212V10.7885H14.5154V2.32212L7.84615 6.825ZM7.84615 5.61346L14.4369 1.21154H1.275L7.84615 5.61346ZM1.17692 2.32212V1.21154V10.7885V2.32212Z"
                  fill="white"
                />
              </svg>
            </span>
            info@dinjan.in
          </div>
          <div className={Styles.infoAndLogoContainer2}>
            <span className={Styles.headerInfoLogo}>
              <svg
                width="15"
                height="15"
                viewBox="0 0 15 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M13.9996 10.733V12.69C14.0003 12.8717 13.963 13.0515 13.8901 13.218C13.8172 13.3845 13.7102 13.5339 13.5761 13.6567C13.442 13.7795 13.2836 13.873 13.1111 13.9312C12.9387 13.9894 12.756 14.0111 12.5747 13.9947C10.5633 13.7766 8.63128 13.0907 6.93379 11.992C5.35449 10.9905 4.01552 9.65414 3.01197 8.07797C1.90731 6.37614 1.21986 4.43855 1.00531 2.42217C0.988971 2.24177 1.01045 2.05996 1.06838 1.88831C1.12631 1.71665 1.21942 1.55892 1.34178 1.42514C1.46413 1.29137 1.61306 1.18449 1.77907 1.1113C1.94509 1.03812 2.12455 1.00023 2.30604 1.00006H4.26695C4.58416 0.996947 4.89169 1.10906 5.13221 1.31549C5.37273 1.52193 5.52983 1.80861 5.57422 2.12209C5.65699 2.74838 5.81048 3.36332 6.03177 3.95517C6.11971 4.18866 6.13874 4.44241 6.08661 4.68637C6.03448 4.93032 5.91337 5.15425 5.73763 5.33161L4.90751 6.16008C5.838 7.79325 7.19292 9.14549 8.82933 10.0741L9.65945 9.24566C9.83717 9.07027 10.0615 8.9494 10.306 8.89737C10.5504 8.84534 10.8047 8.86434 11.0386 8.95211C11.6317 9.17296 12.2478 9.32614 12.8753 9.40874C13.1929 9.45345 13.4828 9.61306 13.6901 9.85723C13.8974 10.1014 14.0076 10.4131 13.9996 10.733Z"
                  stroke="white"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </span>
            99983 91283
          </div>
          <div className={Styles.infoAndLogoContainer}>
            <span className={Styles.headerInfoLogo}>
              <svg
                width="12"
                height="16"
                viewBox="0 0 12 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M4 6C4 6.53043 4.21071 7.03914 4.58579 7.41421C4.96086 7.78929 5.46957 8 6 8C6.53043 8 7.03914 7.78929 7.41421 7.41421C7.78929 7.03914 8 6.53043 8 6C8 5.46957 7.78929 4.96086 7.41421 4.58579C7.03914 4.21071 6.53043 4 6 4C5.46957 4 4.96086 4.21071 4.58579 4.58579C4.21071 4.96086 4 5.46957 4 6Z"
                  fill="white"
                />
                <path
                  d="M6 16C5.61974 16 5.25532 15.8807 4.94619 15.6551C4.66679 15.4512 4.45097 15.1744 4.31859 14.8512L0.894894 9.37443L0.891835 9.36933C0.308384 8.39721 0 7.2848 0 6.15243C0 4.50906 0.624119 2.96404 1.75736 1.80201C2.89061 0.639975 4.39734 0 6 0C7.60266 0 9.10939 0.639975 10.2426 1.80201C11.3759 2.96404 12 4.50906 12 6.15243C12 7.2902 11.689 8.40705 11.1005 9.38223L11.0953 9.39077L7.67644 14.8651C7.54224 15.1855 7.32558 15.4595 7.04656 15.6608C6.73891 15.8827 6.37703 16 6 16ZM1.97177 8.68104L5.45841 14.2586L5.48213 14.3237C5.56456 14.55 5.76785 14.6962 6 14.6962C6.23015 14.6962 6.43293 14.5521 6.51664 14.3292L6.53978 14.2675L10.0218 8.69197C10.4841 7.92404 10.7285 7.04602 10.7285 6.15243C10.7285 4.85732 10.2366 3.63973 9.34353 2.72394C8.45044 1.80818 7.26302 1.30383 6 1.30383C4.73698 1.30383 3.54956 1.80816 2.65645 2.72394C1.76334 3.63971 1.27152 4.85732 1.27152 6.15243C1.27152 7.04147 1.51363 7.91569 1.97177 8.68104Z"
                  fill="white"
                />
              </svg>
            </span>
            3013, Central Bazzar, Varachha Main Road, Surat. 395006
          </div>
        </div>
        <div className={Styles.companyContacts}>
          <div className={Styles.infoAndLogoContainer}>
            <InputGroup />
          </div>
          <img
            src={loginLogo}
            alt=""
            className={Styles.headerInfoLogo}
            srcset=""
          />
          Sign in
        </div>
      </section>

      <section className={Styles.headerMenu}>
        <div className={Styles.companyNameAndLogo}>
          <CgAdidas className={Styles.companyLogo} />
          <div>
            <img src={companyName} alt="company Logo" />
          </div>
        </div>
        <div className={Styles.menuBar}>
          <p>Home</p>
          <p>About Us</p>
          <p>Our Product</p>
          <p>Contact US</p>
          <p className={Styles.logoWrapAtMenu}>
            <AiOutlineHeart className={Styles.logoAtMenu} />
          </p>
          <p className={Styles.logoWrapAtMenu}>
            <BsWallet2 className={Styles.logoAtMenu} />
          </p>
          <div className={Styles.menuBar}>
            <p className={Styles.logoWrapAtMenu}>
              <AiOutlineShoppingCart className={Styles.logoAtMenu} />
            </p>
            <div>
              <p>My Cart</p> <p>0.00</p>
            </div>
          </div>
        </div>
      </section>

      <section className={Styles.attentionGrabber}>
        <p>TEXTILE BRAND IN MARKET</p>
        <h2>A One-Stop solution for all your yarn needs!</h2>
        <span>Get the best quality and assured Lowest Yarn Prices.</span>
        <Buttons buttonStyle="dark" innerText="GET IN TOUCH" />
      </section>

      <section className={Styles.darkSection}>
        <div>
          <img src={bgImageDarksec} alt="" />
        </div>
        <div className={Styles.halfRight}>
          <p className={Styles.whoRWe}>WHO WE ARE</p>
          <h3>The Best Solution for all Industrial & Factory Businesses</h3>
          <p className={Styles.longText}>
            Dinjan is a B2B e-commerce marketplace connecting yarn manufacturers
            and buyers on an innovative technology platform.
          </p>
          <p className={Styles.longText}>
            To be the most Preferred and Convenient Technology driven platform
            across the globe to trade Yarn!
          </p>
          <Buttons buttonStyle="light" innerText="VIEW MORE" />
        </div>
      </section>

      <section className={Styles.secForTrendingProducts}>
        <div className={Styles.trendingProductTitle}>
          <p className={Styles.headingSmall}>Trending Products</p>
          <h2>Trust our Yarn for Unmatched Quality!</h2>
        </div>
        <div className={Styles.cardWrapper}>
          <div className={Styles.trendingMenu}>
            <p>New Product</p>
            <p>Top Selling</p>
            <p>Featured</p>
          </div>

          <div className={Styles.productCard}>
            <Card
              productName="Yarn"
              starRating={zeroStars}
              productImg={product1}
            />
            <Card
              productName="Yarn"
              starRating={fiveStars}
              productImg={product2}
            />
            <Card
              productName="Yarn"
              starRating={fiveStars}
              productImg={product3}
            />
            <Card
              productName="Yarn"
              starRating={zeroStars}
              productImg={product4}
            />
          </div>
        </div>

        <div className={Styles.subSecProducts}>
          <p className={Styles.headingSmall}>Our working process</p>
          <h2>Simple Step to get your Yarn?</h2>
        </div>

        <div className={Styles.circleGroup}>
          <Circle image={letterPad} label="Quotation" circleColor="lighter" />
          <Circle
            image={circle2}
            label="Rate Confirmation"
            circleColor="light"
          />
          <Circle image={circle3} label="Payment" circleColor="dark" />
          <Circle image={circle4} label="Delivery" circleColor="darker" />
        </div>
      </section>

      <section className={Styles.productPromo}>
        <img src={bgImagesec5} alt="" />

        <div className={Styles.contentWrapper}>
          <div className={Styles.wrap}>
            <div className={Styles.promoContentWrap}>
              <img src={bestPriceLogo} alt="BestPriceLogo" />
              <h2>Best Price Guaranteed!</h2>
              <p>
                Get lowest prices with the use of ‘Reverse Bidding’ process.
              </p>
            </div>

            <div className={Styles.promoContentWrap}>
              <img src={qualityAssure} alt="BestPriceLogo" />
              <h2>Quality Assurance</h2>
              <p>
                Excellent workability on Machines and different types of
                qualities are checked for various products.
              </p>
            </div>
          </div>
          <div className={Styles.wrap}>
            <div className={Styles.promoContentWrap}>
              <img src={saveTimeLogo} alt="BestPriceLogo" />
              <h2>Save your Time!</h2>
              <p>
                Get instant quotes from multiple sellers at the same timein a
                single location – No need to call each seller one by one.
              </p>
            </div>

            <div className={Styles.promoContentWrap}>
              <img src={mrktAsrLogo} alt="BestPriceLogo" />
              <h2>Market Intelligence</h2>
              <p>
                Get access to valuable insights, market trends and
                latestindustry news and make better, well-informed buying
                decisions.
              </p>
            </div>
          </div>
        </div>
        <img src={promoSecImage} className={Styles.promoSecImg} alt="" />
      </section>

      <section className={Styles.secForTrendingProducts}>
        <div className={Styles.trendingProductTitle}>
          <p>Dinjan Yarn</p>
          <h2>High-quality Yarn for Exceptional Creations</h2>
        </div>

        <div className={Styles.mainWrapper}>
          <div className={Styles.firstHalf}>
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/c7-iHxGpR-8"
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen
            ></iframe>
          </div>
          <div className={Styles.secHalf}></div>
        </div>
      </section>
    </Fragment>
  );
}

export default LandingPage;
